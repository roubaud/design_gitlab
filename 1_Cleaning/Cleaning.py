import seaborn as sns
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import re


# Define a function to extract latitude and longitude from the given string
def extract_lat_long(point_str):
    '''
    Extrait la latitude et la longitude d'une chaîne au format POINT (6.314944 46.272607)
    :param point_str : string (chaîne de caractère)
    :return : chaînes de caractères (strings) indiquant la longitude et la latitude
    '''
    match = re.match(r'POINT \((\d+\.\d+) (\d+\.\d+)\)', point_str)
    if match:
        latitude = float(match.group(1))
        longitude = float(match.group(2))
        return latitude, longitude
    else:
        return None, None
    
def print_information(df):
    print("date de début :", df.time.min())
    print("date de fin :",df.time.max())
    print(f"Les mois présents sont : {df.time.dt.month.unique()}")
    print(f"La taille du df est de :{df.shape}")
    print(f"il y a {len(df.module_id.unique())} stations")


def import_csv(path):
    '''
    import csv and change the format of the time column to datetime
    '''
    df  = pd.read_csv(path,index_col=False)
    df.time = pd.to_datetime(df["time"],format='%Y-%m-%d %H:%M:%S%z',utc =True)
    return df

def plot_precip(dfm, title ):
    
    sns.displot(data=dfm, x= "vals",kind ="hist",color = "blue",stat = "percent")
    plt.title(title)
    plt.yscale("log") # convert y-axis to Logarithmic scale 
    plt.xlabel("precipitation in mm")
    plt.tight_layout()  # Adjust subplot parameters to give specified padding

def melting_and_plot_precip(df, title="Precipitation values distribution in mm/5min for Oceau stations" ):
    # convert to long (tidy) form
    dfm = df.melt(var_name='station', value_name='vals')
    plot_precip(dfm, title )
    return dfm

