
# Preprocessing des donnees. 
Les donnees entrantes sont stockees dans Data/Raw_data_per_month et les sorties sont stockees dans Clean_data.

### Cleaning.py : 
contain functions called in more than one notebook.

### 0_merging_month.ipynb :
fusionne les données telechargées dans Extraction des donnes et stockees dans Raw_data_per_month en un seul csv.
**Input**: monthly raw data
**Output**:netatmo_all_month.csv

### 1_cleaning_data.ipynb :
Applique un filtre spatial, supprime les duplicatas, et remplace les ':' des noms de stations en '_'.
**Input**: netatmo_all_month.csv
**Output**: data_total_month_clean_2023.csv'

### 2.1_data_cleaning_pws-pyqc.ipynb : 
Formatte les donnes pour avoir une donnee par heure et par station. Attention certaines stations peuvent ne pas avoir de donnees a 15 min de l'heure pile et disparaissent durant cette aggregation. 
**Input** : data_total_month_clean_2023.csv
**Output** :  pws_hourly_total.csv

### 2.2_data_formatting_Meta.ipynb : 
Creation d'un csv contenant pour chaque station Netatmo leur latitude, longitude, et altitude.
**Input**: data_total_month_clean_2023.csv, pws_hourly_total.csv
**Output**:
   - metatableGen_tot.csv : csv contenant toutes les stations sur l'année
   - metatablehour_tot.csv : csv contenant les stations ayant des donnees a moins de 15min d'une heure pile.

### 3.1_data_formatting_Oceau_Metadata.ipynb : 
Creation d'un csv contenant pour chaque station Netatmo leur latitude et longitude en wgsS84 a partir de coordonnees LV95 (suisse).
**Input**: Metadata_Oceau_LV95_coordinate.csv csv creer a partir d'information trouvee sur https://vhg.ch/xt_vh_718536/station_list.php?cfg=display_VH_PLUVIO*mode_list
**Output**: Metadata_oceau.csv

### 3.2_data_cleaning_oceau_algo_2.ipynb
Conversion des donnees prevalidees de l'Oceau de mm/h sur 5 min a des mm sommees sur l'heure. Le csv de sortie ne contient qu'une donnee par heure.
**Input**: Pluie5Min.xlsx (donnee fournie par l'Oceau)
**Output**: oceau_hour_after_april.csv, oceau_hour_after_april.csv