# Projet d'Analyse des Données Pluviométrique Netatmo sur le Canton de Genève

Ce projet a été effectué dans le cadre d'un [design project à l'epfl](https://www.epfl.ch/schools/enac/education/environmental-sciences-and-engineering/environmental-sciences-and-engineering/formation-en/master-en/projets/design-project/) en collaboration avec l'office cantonal de l'eau du canton de Genève. L'objectif de ce projet est d'implémenter les algorithmes de controle suivants sur les données de l'Oceau du canton de Genève:
 - PWSQC proposé par [De Vos et Al.](https://doi.org/10.1029/2019GL083731) disponible sur https://github.com/LottedeVos/  
 - PWS-pyqc proposé par [Bardossy et Al.](https://doi.org/10.5281/zenodo.4501919) disponible sur https://github.com/AbbasElHachem/pws-pyqc
 
Ce projet contient divers sous-dossiers et fichiers permettant l'extraction, le nettoyage, l'application d'algorithmes spécifiques, et l'analyse de données stockées sur le datalake du canton de Genève.

## Structure du Projet
```
├───0_Extraction_des_donnees
├───1_Cleaning
├───2_Algo1_PWSQC-master
│   ├───InputFiles
│   └───OutputFolder
├───3_Analyses
├───Algo2_PWSPYQC_master
│   └───Geneve
├───Data
│   ├───Clean_data #Donnee de precipitation nettoyee par le dossier cleaning
│   ├───Output_algo_1
│   ├───Output_algo_2
│   └───Raw_data_per_month # Donnee extraite par le .ipynb d'Extraction_des_donnes contenant les donnees brutes de precipitation extrait de l'API
├───fig
```

- **0_Extraction_des_donnees** : 
  - Contient du code Python pour télécharger des données depuis le datalake du canton de Genève.

- **1_Cleaning** : 
  - Contient du code Python pour nettoyer et mettre en forme les données pour les algorithmes PWSQC et PWS_pyqc.
  - Ce dossier contient son propre `README.md`.

- **2_Algo1_PWSQC-master** : 
  - Implémentation en R de l'algorithme proposé par De Vos et al.
  - Ce dossier contient son propre `README.md`.

- **Algo2_PWSPYQC_master** : 
  - Implémentation en Python de l'algorithme proposé par Bardossy et al.
  - Ce dossier contient son propre `README.md`.

- **3_Analyses** : 
  - Contient du code python pour l'analyse des données avant et après l'application des algorithmes.
  - Principal fichier : `Data_analysis.ipynb`

## Autres Fichiers et Dossiers

- **Funcs.py** : 
  - Contient des fonctions utilitaires utilisées dans divers scripts du projet.

- **data** : 
  - Dossier pour stocker les fichiers de données nécessaires au projet.

- **fig** : 
  - Dossier pour stocker les figures générées par les analyses.

## .gitignore

Le fichier `.gitignore` est configuré pour exclure :
- Toutes les images et fichiers CSV ainsi que le contenu des dossiers `fig` et `data`.

### Prérequis

- Python 3.x
- R
- Les bibliothèques Python listées dans `0_extraction_des_donnees/requirements.txt`

### Installation

1. Cloner le dépôt :
    ```bash
    git clone https://gitlab.epfl.ch/roubaud/design_gitlab
    cd votre-projet
    ```

2. Installer les dépendances Python :
    ```bash
    pip install -r requirements.txt
    ```

## Contribuer

Les contributions sont les bienvenues ! Merci de bien vouloir ouvrir une issue avant de soumettre une pull request.


## Auteurs

- [Ariane Augustoni](https://gitlab.epfl.ch/aaugusto) : ariane.augustoni@gmail.com
- [Roubaud Yann](https://gitlab.epfl.ch/roubaud) : yroubaud@hotmail.fr

## Remerciements

Nous tenons à exprimer notre gratitude à Mr. Ion Iorgulescu pour le temps qu'il a consacré à notre encadrement et pour ses réponses toujours très réactives. Nous remercions également Pr. Alexis Berne pour sa réactivité et ses précieux conseils. Enfin, nous adressons nos remerciements aux services de Netatmo pour leurs échanges de mails et leur collaboration.

