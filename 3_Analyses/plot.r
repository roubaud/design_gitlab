


rm(list=ls())
require(TeachingDemos)	# needed for subplot()


Ndataset <- read.csv("Data/final_filtered_strict.csv", sep=",")
Ndataset <- as.matrix(Ndataset)
Rdataset <- read.csv("Data/Output_algo2/constructed_reference_data_algo2.csv", sep=",")
Rdataset <- as.matrix(Rdataset)



lim <- c(0,1000)  # if needed change upper boundary to maximum cumulative rainfall over the selected period

# # period and station information # #

#Entire data
# Including hours in the time format
starttime_with_leadtime <- strptime("2023-04-01 09:00:00", format="%Y-%m-%d %H:%M:%S", tz="GMT")
starttime <- strptime("2023-04-01 09:00:00", format="%Y-%m-%d %H:%M:%S", tz="GMT")
endtime <- strptime("2024-01-01 00:00:00", format="%Y-%m-%d %H:%M:%S", tz="GMT")


Meta <- read.table("Data/Output_algo2/Meta_pws_end_algo2.csv", header=T, sep=",")

# leave out the first month lead-time in the figures: MODIFIED: leave out the first day
Time <- seq(starttime_with_leadtime, endtime, by="1 h")[-1]
Ndataset <- Ndataset[which(Time > starttime)[1] : which(Time == endtime),]
Rdataset <- Rdataset[which(Time > starttime)[1] : which(Time == endtime),]


png(paste0("Figure/algo1et2.png"), width=1080, height=540)

par(mar=c(5.1,5.1,4.1,1.1))
layout(matrix(c(1,2),1,2,TRUE))	# needed to make subplot in right window
plot(NA, NA, xlab="Précipitations références cumulés (mm)", ylab="Précipitations Netatmo cumulés (mm)", xlim=lim, ylim=lim, main="Données acceptées par les deux algorithmes", cex.lab=2, cex.axis=1.5, cex.main=1.8)
abline(0, 1, col="grey", lty=2, lwd=2)

for(i in 1:nrow(Meta)){
  R <- Rdataset[,i]
  N <- Ndataset[,i]
  
  selec <- which(is.na(R) | is.na(N))
  #FZflag <- FZ_flags[-selec,i]
  #HIflag <- HI_flags[-selec,i]
  #SOflag <- SO_flags[-selec,i]
  
  N <- N[-selec]
  R <- R[-selec]
  
  
  Raccum <- cumsum(R)
  print(length(Raccum))
  Naccum <- cumsum(N)
  
  
  print(paste("i =", i, "; ID =", Meta$ID[i]))
  
  lines(Raccum, Naccum, lwd=0.5, col="blue")
  
}
fracremain <- rep(0, times=nrow(Meta))


rect(max(lim)*0.05, max(lim)*0.6,  max(lim)*0.4, max(lim), col = "white", border=NA)	# make white background for subplot
#subplot( boxplot(corlist, fracremain, names=c("correlation","fraction"), cex.axis=1, col=c("lightslateblue", "lightblue"), ylim=c(0,1), yaxs="i"), x=c(max(lim)*0.05,max(lim)*0.4), y=c(max(lim)*0.6,max(lim)))

dev.off()



