#  Personal Weather Station (PWS) quality control using python on Genevra case study.
 -----------------------------------------------------------------------------------------------

 This folder use the code of Bardossy et Al. (https://doi.org/10.5281/zenodo.4501919) available on https://github.com/AbbasElHachem/pws-pyqc.

The PWSpyqcFunctions.py file contains the function implemented by Bardossy and al. 
The pwspyqc_processing.ipynb jupyter notebook is the implementation of the Geneva case study in the context of the design project.
