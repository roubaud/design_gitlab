from datetime import datetime
from typing import Tuple, List, Dict, Optional, Union
from pydantic import BaseModel, ValidationError, ConfigDict, model_validator, conlist

class MyBaseModel(BaseModel):
    model_config = ConfigDict(strict=True)

class NALocation(MyBaseModel):
    location: conlist(item_type=float, min_length=2, max_length=2)
    timezone: str
    country: str
    altitude: int
    city: Optional[str] = None
    street: Optional[str] = None

class NATemperatureAndHumidityMeasure(MyBaseModel):
    res: Dict[str, list]
    type: list

    @model_validator(mode='after')
    def check_type(self) -> 'NATemperatureAndHumidityMeasure':
        type = self.type

        if type != ['temperature', 'humidity']:
            raise ValueError('Not a valid temperature and humidity measure')

        return self

    @model_validator(mode='after')
    def check_number_of_res_and_types_match(self) -> 'NATemperatureAndHumidityMeasure':
        res = self.res
        type = self.type

        if len(res.keys()) != 1:
            raise ValueError('...')
        
        if len(list(res.values())[0]) != 2:
            raise ValueError('Number of results do not match types.')
            
        return self

class NAPressureMeasure(MyBaseModel):
    res: Dict[str, list]
    type: list

    @model_validator(mode='after')
    def check_type(self) -> 'NAPressureMeasure':
        type = self.type

        if type != ['pressure']:
            raise ValueError('Not a valid pressure measure')

        return self
    
    @model_validator(mode='after')
    def check_number_of_res_and_types_match(self) -> 'NAPressureMeasure':
        res = self.res
        
        if len(res.keys()) != 1:
            raise ValueError('...')
        
        if len(list(res.values())[0]) != 1:
            raise ValueError('Number of results do not match types.')
        
        return self

class NARainMeasure(MyBaseModel):
    rain_60min: float
    rain_24h: float
    rain_live: float
    rain_timeutc: int

class NAWindMeasure(MyBaseModel):
    wind_strength: int
    wind_angle: int
    gust_strength: int
    gust_angle: int
    wind_timeutc: int
    
class NARecord(MyBaseModel):
    _id: str
    place: NALocation
    mark: int
    measures: Dict[str, Union[
        NATemperatureAndHumidityMeasure, 
        NAPressureMeasure, 
        NARainMeasure, 
        NAWindMeasure
    ]]
    modules: List[str]
    module_types: dict

    @model_validator(mode='after')
    def check_number_of_modules_and_types_match(self) -> 'NARecord':
        modules = self.modules
        module_types = self.module_types

        if len(module_types.keys()) != len(modules):
            raise ValueError('Number of modules and module types do not match.')
        return self

class NADataFile(MyBaseModel):
    status: str
    time_server: int
    body: List[NARecord]

class Measure(BaseModel):
    measurement: str
    time: datetime
    value: float
    