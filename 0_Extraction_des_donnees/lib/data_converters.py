import lib.data_models as data_models


class UnknownMeasureType(Exception):
    "..."
    pass


def na_measure_converter(na_measure):
    
    na_measure_type = type(na_measure)
    
    if na_measure_type in [data_models.NATemperatureAndHumidityMeasure, data_models.NAPressureMeasure]:

        time = list(na_measure.res.keys())[0]
        values = list(na_measure.res.values())[0]
        
        return [ data_models.Measure(time=time, measurement=measurement, value=values[i]) for i, measurement in enumerate(na_measure.type) ]
        
    elif na_measure_type == data_models.NARainMeasure:

        time = na_measure.rain_timeutc

        return [ data_models.Measure(time=time, measurement=m, value=v) for (m, v) in na_measure if m != "rain_timeutc" ]

    elif na_measure_type == data_models.NAWindMeasure:

        time = na_measure.wind_timeutc
        
        return [ data_models.Measure(time=time, measurement=m, value=v) for (m, v) in na_measure if m != "wind_timeutc" ]
        
    else:
        raise UnknownMeasureType


def na_data_file_body_converter(na_data_file_body):

    for in_record in na_data_file_body:

        place_dict = in_record.place.model_dump()
        module_types = in_record.module_types

        out_record_dict = [{
                "module_id": module_id, 
                "module_type": module_types.get(module_id, 'unknown'), 
                **place_dict, 
                **data.model_dump()
            } for (module_id, m) in in_record.measures.items() for data in na_measure_converter(m)
        ]
    
        yield from out_record_dict