import seaborn as sns

import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 10}) # Set plot font size to 16pt

import numpy as np
import time
from scipy.spatial import cKDTree

from pykrige import OrdinaryKriging as OKpy

import tqdm

import pandas as pd
import statsmodels.api as sm
from scipy import spatial as scsp

from pykrige.ok import OrdinaryKriging as OKpy



def QQplot( sample1,sample2, num_quantiles = 100, label_1_sample = "Oceau", label_2_sample = "Netatmo"):
    '''
    effectue un diagramme quantile-quantile affichant un nombre de quantile nom_quantiles
    '''
    # Create Q-Q plot
    fig, ax = plt.subplots(figsize=(6, 6))    

    # Calculate quantiles for each sample
    quantiles_data1 = np.quantile(sample1, np.linspace(0, 1, num_quantiles))
    quantiles_data2 = np.quantile(sample2, np.linspace(0, 1, num_quantiles))
    
    # Create QQ plot using the selected quantiles
    fig = sm.qqplot_2samples(quantiles_data1, quantiles_data2,line = "45", ax=ax)

    #plt.text( 10,0.9, s=f"number of 0 quantiles in {label_1_sample}: {len(quantiles_data1[quantiles_data1 == 0])}")
    plt.xlabel(label_1_sample)
    plt.ylabel(label_2_sample)
    
    #plt.title(f"QQplot comparant {num_quantiles} quantiles entre les précipitations de {label_1_sample} et {label_2_sample}")
    plt.show()

def double_mass_analysis(precip1, precip2):
    """
    Implémente la méthode des doubles cumuls pour comparer deux séries de données de précipitations.

    Parameters
    ----------
    :precip1: Série de précipitations de la station 1.
    :precip2: Série de précipitations de la station 2.

     Returns
    ----------
    :cum_precip1: Cumuls successifs de la station 1
    :cum_precip2: Cumuls successifs de la station 2
    """
    # Calcul des cumuls successifs
    cum_precip1 = np.cumsum(precip1)
    cum_precip2 = np.cumsum(precip2)
    
    return cum_precip1, cum_precip2

def plot_double_mass_curve(cum_precip1, cum_precip2,label_station1,label_station2,xmin,xmax):
    """
    Trace la courbe des doubles cumuls pour visualiser la cohérence des données.

    Parameters
    ----------
    :cum_precip1: Cumuls successifs de la station 1.
    :cum_precip2: Cumuls successifs de la station 2.
    """
    plt.figure(figsize=(10, 6))
    plt.plot(cum_precip2, cum_precip1, marker='o', linestyle='-', color='b')
    plt.xlabel(f'Somme cumulée de la station Oceau {label_station2}')
    plt.ylabel(f'Somme cumulée de la station Netatmo {label_station1}')
    #plt.title('Courbe des doubles cumuls')
    plt.ylim([xmin,xmax])
    plt.xlim([xmin,xmax])
    plt.grid(True)
    plt.axline((0, 0), slope=1, color='r', linestyle='--')
    plt.show()

def create_cumsum_df(df_pws_flagged,in_primary_pcp,idx_pws,idx_prim):
    # Fusionner les DataFrames sur les dates communes
    df_merged = pd.merge(df_pws_flagged[idx_pws], in_primary_pcp[idx_prim], how='inner',  left_index=True, right_index=True).dropna()

    # Calculer les cumuls successifs
    df_merged['Cumulative_pws'] = df_merged[idx_pws].cumsum()
    df_merged['Cumulative_prim'] = df_merged[idx_prim].cumsum()

    # Affichage du DataFrame fusionné
    return df_merged

def return_xlon_xlat(longitude, latitude):
    x_lon = np.linspace(min(longitude), max(longitude), 50)
    y_lat = np.linspace(min(latitude), max(latitude), 50)
    return x_lon,y_lat

def find_lon_lat_precip(df_pcp,df_coords, idx_time = "2023-09-13 11:00:00"):
    '''
    récupère la longitude, la latitude, et les précipitations en mm des stations d'un dataframe au temps idx_time
    dans l'objectif d'effetuer une interpolation ensuite. 

    Parameters
    ----------
    :df_pcp: 'dataframe' 
    chaque colonne représente une station avec des valeurs de précipitation en mm
    :df_coords: 'dataframe' 
    contient les informations meta de chaque station, sa longitude et sa latitude
    :idx_time: 'date time string'
    heure à laquelle les informations veulent être récupérées.

    Returns
    ----------
    :longitude: array contenant les longitudes de chaque stations ayant des données au temps idx_time
    :latitude: array contenant les latitudes de chaque stations ayant des données au temps idx_time
    :precipitation: array contenant les précipitations en mm de chaque stations ayant des données au temps idx_time

    '''

    # Extract coordinates and values
    meta = df_coords[df_coords.index.isin(df_pcp.loc[idx_time].dropna().index)]
    print(f"il y {len(meta)} stations acceptées le {idx_time}")

    longitude = meta['lon'].values
    latitude= meta['lat'].values
    precipitation = np.array(df_pcp.loc[idx_time].dropna(), dtype=float)
    return longitude,latitude, precipitation

def ordinary_krig(longitude, latitude, precipitation,x_lon,y_lat):
    '''
    Performe un krigeage ordinaire sur N stations. Le variogramme utilisé est sphérique avec un nugget de 0. 

    Parameters
    ----------
    :longitude: 'array' [Nx1]
    array contenant les longitudes de chaque stations ayant des données au temps de l'interpolation
    :latitude: 'array' [Nx1]
    rray contenant les latitudes de chaque stations ayant des données au temps de l'interpolation
    :precipitation: 'array' [Nx1]
    array contenant les précipitations en mm de chaque stations ayant des données au temps idx_time de l'interpolation
    :x_lon:  'array'
    valeurs de longitude sur lesquelles on veut effectuer l'interpolation
    :y_lat: 'array'
    valeurs de latitude sur lesquelles on veut effectuer l'interpolation

    Returns
    ----------
    :z: Les valeurs interpolées danns la grille de x_lon, y_lat

    '''
    # Perform ordinary kriging
    OK = OKpy(
        longitude, latitude, precipitation,
        variogram_model='spherical',
        verbose=False,
        enable_plotting=False,
        variogram_parameters={
            'sill': np.nanvar(precipitation),
            'range': 30000,
            'nugget': 0},
        exact_values=True)
    z, ss = OK.execute(style = 'grid',mask =None, xpoints= x_lon, ypoints= y_lat)
    return z

def plot_interpolation(precipitation, longitude ,latitude, x_lon,y_lat,label = "Netatmo" ):
    #plot an ordinary interpolation to compare the data

    grid_lon, grid_lat = np.meshgrid(x_lon, y_lat)

    z = ordinary_krig(longitude, latitude, precipitation,  x_lon,y_lat)
    # Step 4: Plot the results
    plt.figure(figsize=(10, 8))
    contour = plt.contourf(grid_lon, grid_lat, z, cmap='viridis')
    plt.colorbar(contour, label='Precipitation en mm')
    plt.scatter(longitude, latitude, c='red', marker='o', label=label)
    plt.xlabel('Longitude')
    plt.ylabel('Latitude')
    #plt.title('Precipitation Interpolation using Ordinary Kriging')
    plt.legend()
    plt.show()

def plot_combined_interp(longitude_pws,longitude_oceau,latitude_pws, latitude_oceau,
                         precipitation_pws,precipitation_oceau,x_lon,y_lat):
    '''
    Affiche l'interpolation par krigeage ordinaire de deux types de stations ensemble.  
    Typiquement montre l'interpolation à la fois sur les stations de l'Oceau et de Netatmo
    '''
    # Combine the data for interpolation
    combined_longitude = np.concatenate((longitude_pws, longitude_oceau))
    combined_latitude = np.concatenate((latitude_pws, latitude_oceau))
    combined_precipitation = np.concatenate((precipitation_pws, precipitation_oceau))

    #effectue le krigeage ordinaire
    z = ordinary_krig(combined_longitude, combined_latitude, combined_precipitation,x_lon,y_lat)
    grid_lon, grid_lat = np.meshgrid(x_lon, y_lat)

    # affiche le krigeage ordinaire
    plt.figure(figsize=(10, 8))
    contour = plt.contourf(grid_lon, grid_lat, z, cmap='viridis')
    plt.colorbar(contour, label='Precipitation in mm')
    plt.scatter(longitude_pws, latitude_pws, c='red', marker='o', label='Netatmo')
    plt.scatter(longitude_oceau, latitude_oceau, c='blue', marker='o', label='Oceau')
    plt.xlabel('Longitude')
    plt.ylabel('Latitude')
    plt.legend()
    plt.show()


def compute_0_percent(ppt_data):
    origin_count = ppt_data.size  # shape[0]
    count_below_thr = ppt_data[ppt_data == 0].size
    p0 = np.divide(count_below_thr, origin_count)
    return p0

def give_info_for_table(df, label ):
    '''
    output a dataframe containing statistical information about the df. 
    all statistics except count, p0 and pNan are computed without 0 mm precipitation.

    input:

    :df: dataframe containing precipitation values in mm in a column named vals
    :label: name of the index of df_stat. In this context the type of station where there are from.

    output:
    :df_stat: dataframe giving  statistics about the precipitation distribution

    '''
    df_without_0 = df[df.vals>0]
    print("All statistics except count, p0 and pNan are computed without 0 mm precipitation.")
    df_stat = pd.DataFrame(df_without_0.describe()).transpose()
    df_stat["p0"] =compute_0_percent(df.vals.dropna())
    df_stat["pNan"] = df.vals.isna().sum()/df.vals.shape[0]*100
    df_stat["count"] = df.vals.count().sum()
    df_stat.index = [label]
    return df_stat